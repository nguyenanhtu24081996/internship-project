-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th2 19, 2020 lúc 07:48 AM
-- Phiên bản máy phục vụ: 10.4.6-MariaDB
-- Phiên bản PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `thuctap`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `loaitin`
--

CREATE TABLE `loaitin` (
  `ID` int(11) NOT NULL,
  `IDTheLoai` int(11) DEFAULT NULL,
  `TenLoaiTin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `loaitin`
--

INSERT INTO `loaitin` (`ID`, `IDTheLoai`, `TenLoaiTin`) VALUES
(1, 1, 'Chống tham nhũng'),
(2, 1, 'Quốc hội'),
(3, 1, 'An toàn giao thông'),
(4, 1, 'Môi trường'),
(5, 2, 'Tài chính'),
(6, 2, 'Đầu tư'),
(7, 2, 'Thị trường'),
(8, 2, 'Doanh nhân'),
(9, 3, 'Thời trang '),
(10, 3, 'Nhạc '),
(11, 3, 'Phim'),
(12, 3, 'Truyền hình'),
(13, 4, 'Việt Nam và thế giới'),
(14, 4, 'Quân Sự'),
(15, 4, 'Thế giới đó đây'),
(16, 4, 'Bình luận quốc tế'),
(17, 5, 'Tuyển sinh'),
(18, 5, 'Du học'),
(19, 5, 'Học tiếng anh'),
(20, 5, 'Khoa học'),
(21, 6, 'Gia đình'),
(22, 6, 'Giới trẻ'),
(23, 6, 'Mẹ và bé'),
(24, 6, 'Du lịch'),
(25, 7, 'Hồ sơ vụ án'),
(26, 7, 'Tư vấn pháp luật'),
(27, 7, 'Ký sự pháp đình'),
(28, 8, 'Bóng đá Việt Nam'),
(29, 8, 'Bóng đá thế giới'),
(30, 9, 'Sản phẩm'),
(31, 9, 'Ứng dụng'),
(32, 9, 'Tin công nghệ'),
(33, 10, 'Làm đẹp'),
(34, 10, 'Các bệnh'),
(35, 10, 'Tư vấn sức khỏe');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `theloai`
--

CREATE TABLE `theloai` (
  `ID` int(11) NOT NULL,
  `TenTheLoai` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `theloai`
--

INSERT INTO `theloai` (`ID`, `TenTheLoai`) VALUES
(1, 'Thời sự'),
(2, 'Kinh doanh'),
(3, 'Giải trí'),
(4, 'Thế giới'),
(5, 'Giáo dục'),
(6, 'Đời sống'),
(7, 'Pháp Luật'),
(8, 'Thể thao'),
(9, 'Công nghệ'),
(10, 'Sức Khỏe');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tintuc`
--

CREATE TABLE `tintuc` (
  `ID` int(11) NOT NULL,
  `IDLoaitin` int(11) DEFAULT NULL,
  `NoiDung` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TieuDe` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NgayTao` datetime DEFAULT NULL,
  `TacGia` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IMG` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_vietnamese_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

CREATE TABLE `user` (
  `ID` int(11) NOT NULL,
  `user` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ten` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `user`
--

INSERT INTO `user` (`ID`, `user`, `password`, `ten`) VALUES
(1, 'admin', '12345', 'admin'),
(2, 'nguyenanhtu', '12345', 'Nguyễn Anh Tú'),
(3, 'daoxuanngoc', '12345', 'Đào Xuân Ngọc'),
(4, 'nguyenvanhuan', '12345', 'Nguyễn Văn Huân'),
(5, 'dinhanhdung', '12345', 'Đinh Anh Dũng');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `loaitin`
--
ALTER TABLE `loaitin`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `IDTheLoai` (`IDTheLoai`);

--
-- Chỉ mục cho bảng `theloai`
--
ALTER TABLE `theloai`
  ADD PRIMARY KEY (`ID`);

--
-- Chỉ mục cho bảng `tintuc`
--
ALTER TABLE `tintuc`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `IDLoaitin` (`IDLoaitin`);

--
-- Chỉ mục cho bảng `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `loaitin`
--
ALTER TABLE `loaitin`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT cho bảng `theloai`
--
ALTER TABLE `theloai`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `tintuc`
--
ALTER TABLE `tintuc`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `user`
--
ALTER TABLE `user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `loaitin`
--
ALTER TABLE `loaitin`
  ADD CONSTRAINT `loaitin_ibfk_1` FOREIGN KEY (`IDTheLoai`) REFERENCES `theloai` (`ID`);

--
-- Các ràng buộc cho bảng `tintuc`
--
ALTER TABLE `tintuc`
  ADD CONSTRAINT `tintuc_ibfk_1` FOREIGN KEY (`IDLoaitin`) REFERENCES `loaitin` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
